package com.unimed.medicosespecialidadesapi.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.unimed.medicosespecialidadesapi.errors.ErrorResponse;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleException(Exception e) {
		HttpStatus httpStatus = HttpStatus.BAD_GATEWAY;
		return new ResponseEntity<>(
				new ErrorResponse(e.getLocalizedMessage(), httpStatus.value(), httpStatus.getReasonPhrase()),
				httpStatus);
	}
}