package com.unimed.medicosespecialidadesapi.errors;

public class ErrorResponse {
	
	private String message;
    private int code;
    private String status;
 
	public ErrorResponse(String message, int code, String status) {		
		this.message = message;
		this.code = code;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
