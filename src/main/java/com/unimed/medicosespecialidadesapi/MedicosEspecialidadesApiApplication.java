package com.unimed.medicosespecialidadesapi;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.unimed.medicosespecialidadesapi.entity.Especialidade;
import com.unimed.medicosespecialidadesapi.entity.Medico;
import com.unimed.medicosespecialidadesapi.repositories.EspecialidadeRepository;
import com.unimed.medicosespecialidadesapi.repositories.MedicoRepository;

@SpringBootApplication
public class MedicosEspecialidadesApiApplication {

	@Autowired
	private EspecialidadeRepository eRepository;

	@Autowired
	private MedicoRepository mRepository;

	public static void main(String[] args) {
		SpringApplication.run(MedicosEspecialidadesApiApplication.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
		return () -> {
			Especialidade cardiologia = new Especialidade(null, "Cardiologia",
					"Trata de doenças e problemas relacionados ao coração e à circulação sanguínea", true);
			Especialidade dermatologia = new Especialidade(null, "Dermatologia",
					"Trata de doenças da pele e seus anexos", true);
			Especialidade neurologia = new Especialidade(null, "Neurologia",
					"Trata dos aspectos clínicos dos problemas no sistema nervoso", true);
			eRepository.save(cardiologia);
			eRepository.save(dermatologia);
			eRepository.save(neurologia);			
			
			Medico tony = new Medico(null, "Tony Stark", "01/12/1985",true, cardiologia);
			Medico peter = new Medico(null, "Peter Parker", "01/12/1993",true, cardiologia);
			Medico steve = new Medico(null, "Steve Rogers", "01/12/1993",true, cardiologia);
			Medico carol = new Medico(null, "Carol Danvers", "01/12/1988",true, neurologia);
			mRepository.save(tony);
			mRepository.save(peter);
			mRepository.save(steve);
			mRepository.save(carol);
		};
	}
}
