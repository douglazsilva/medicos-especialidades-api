package com.unimed.medicosespecialidadesapi.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unimed.medicosespecialidadesapi.entity.Especialidade;
import com.unimed.medicosespecialidadesapi.entity.Medico;

@Repository
public interface MedicoRepository extends JpaRepository<Medico, Long> {
	List<Medico> findByEspecialidade(Especialidade especialidade);
}
