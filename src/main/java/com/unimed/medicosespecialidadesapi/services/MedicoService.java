package com.unimed.medicosespecialidadesapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.unimed.medicosespecialidadesapi.entity.Especialidade;
import com.unimed.medicosespecialidadesapi.entity.Medico;
import com.unimed.medicosespecialidadesapi.repositories.MedicoRepository;

@Service
public class MedicoService {

	@Autowired
	private MedicoRepository repository;

	public List<Medico> findAll() {
		return repository.findAll();
	}

	public Medico findById(Long id) {
		Optional<Medico> medico = repository.findById(id);
		if (!medico.isPresent())
			throw new RuntimeException("id: " + id + " não encontrado");
		return medico.get();
	}
	
	public List<Medico> findByEspecialidade(Long idEspecialidade) {
		Especialidade e = new Especialidade();
		e.setId(idEspecialidade);
		return repository.findByEspecialidade(e);
	}

	public Medico save(@RequestBody Medico medico) {
		return repository.save(medico);
	}

	public ResponseEntity<Object> update(Long id, Medico medico) {
		Optional<Medico> atualizacao = repository.findById(id);
		if (!atualizacao.isPresent())
			ResponseEntity.notFound().build();
		medico.setId(id);
		repository.save(medico);
		return ResponseEntity.noContent().build();
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}	
}
