package com.unimed.medicosespecialidadesapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.unimed.medicosespecialidadesapi.entity.Especialidade;
import com.unimed.medicosespecialidadesapi.repositories.EspecialidadeRepository;

@Service
public class EspecialidadeService {

	@Autowired
	private EspecialidadeRepository repository;

	public List<Especialidade> findAll() {
		return repository.findAll();
	}

	public Especialidade findById(Long id) {
		Optional<Especialidade> especialidade = repository.findById(id);
		if (!especialidade.isPresent())
			throw new RuntimeException("id-" + id);
		return especialidade.get();
	}

	public Especialidade save(Especialidade especialidade) {
		return repository.save(especialidade);
	}

	public ResponseEntity<Object> update(Long id, Especialidade especialidade) {
		Optional<Especialidade> atualizacao = repository.findById(id);
		if (!atualizacao.isPresent())
			ResponseEntity.notFound().build();
		especialidade.setId(id);
		repository.save(especialidade);
		return ResponseEntity.noContent().build();
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
}
