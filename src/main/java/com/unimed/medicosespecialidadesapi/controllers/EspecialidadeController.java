package com.unimed.medicosespecialidadesapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.unimed.medicosespecialidadesapi.entity.Especialidade;
import com.unimed.medicosespecialidadesapi.services.EspecialidadeService;

@RestController
@RequestMapping("/especialidades")
public class EspecialidadeController {

	@Autowired
	private EspecialidadeService service;

	@GetMapping
	public List<Especialidade> listarEspecialidades() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	public Especialidade buscarEspecialidade(@PathVariable Long id) {
		return service.findById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Especialidade adicionar(@RequestBody Especialidade especialidade) {
		return service.save(especialidade);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> atualizar(@PathVariable Long id, @RequestBody Especialidade especialidade) {
		return service.update(id, especialidade);
	}

	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		service.delete(id);
	}
}
