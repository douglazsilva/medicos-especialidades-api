package com.unimed.medicosespecialidadesapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.unimed.medicosespecialidadesapi.entity.Medico;
import com.unimed.medicosespecialidadesapi.services.MedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private MedicoService service;

	@GetMapping
	public List<Medico> listarMedicos() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	public Medico buscarMedico(@PathVariable Long id) {
		return service.findById(id);
	}
	
	@GetMapping("/especialidade/{idEspecialidade}")
	public List<Medico> buscarMedicoPorEspecialidade(@PathVariable Long idEspecialidade) {
		return service.findByEspecialidade(idEspecialidade);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Medico adicionar(@RequestBody Medico medico) {
		return service.save(medico);
	}

	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> atualizar(@PathVariable Long id, @RequestBody Medico medico) {
		return service.update(id, medico);
	}

	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		service.delete(id);
	}
}
